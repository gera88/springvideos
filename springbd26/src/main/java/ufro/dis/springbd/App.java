package ufro.dis.springbd;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ufro.dis.beans.Equipo;
import ufro.dis.beans.Jugador;
import ufro.dis.beans.Marca;
import ufro.dis.service.ServiceJugador;
import ufro.dis.service.ServiceMarca;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		ApplicationContext appContect = new ClassPathXmlApplicationContext("ufro/dis/xml/beans.xml");
		ServiceJugador sm = (ServiceJugador) appContect.getBean("serviceJugadorImpl");
		//Marca mar4 = (Marca) appContect.getBean("marca4");
		//Equipo eq1 = (Equipo) appContect.getBean("equipo1");
		Jugador jugador = (Jugador) appContect.getBean("jugador1");
		
		try {
			// sm.registar(mar4);
			sm.registrar(jugador);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
