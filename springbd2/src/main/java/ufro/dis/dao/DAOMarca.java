package ufro.dis.dao;

import ufro.dis.beans.Marca;

public interface DAOMarca {
	
	public void registrar(Marca marca) throws Exception;

}
