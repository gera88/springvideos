package ufro.dis.springbd;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ufro.dis.beans.Marca;
import ufro.dis.service.ServiceMarca;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		Marca marca = new Marca();
		marca.setId(3);
		marca.setNombre("MarcaTres");

		ApplicationContext appContect = new ClassPathXmlApplicationContext("ufro/dis/xml/beans.xml");
		ServiceMarca sm= (ServiceMarca)appContect.getBean("serviceMarcaImpl");
		Marca mar4= (Marca)appContect.getBean("marca4");
		try {
			sm.registar(mar4);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
