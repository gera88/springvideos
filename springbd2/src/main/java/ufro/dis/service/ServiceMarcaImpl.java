package ufro.dis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ufro.dis.beans.Marca;
import ufro.dis.dao.DAOMarca;
@Service
public class ServiceMarcaImpl implements ServiceMarca {
	@Autowired
	private DAOMarca daoMarca;
	
	@Override
	public void registar(Marca marca) throws Exception {
		 
		try {
			daoMarca.registrar(marca);
		} catch (Exception e) {
			throw e;
		}
		
		
		
	}

	
}
