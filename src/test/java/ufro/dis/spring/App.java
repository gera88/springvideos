package ufro.dis.spring;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ufro.dis.beans.Barcelona;
import ufro.dis.beans.Ciudad;
import ufro.dis.beans.Jugador;
import ufro.dis.beans.Juventus;
import ufro.dis.beans.Persona;
import ufro.dis.interfaces.IEquipo;

public class App {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Elija un equipo: 1-Barcelona ; 2-Juventus");
		int respuesta = sc.nextInt(); 
		ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		Jugador jug = (Jugador)appContext.getBean("jugador1");
		
		switch (respuesta) {
		case 1:
			jug.setEquipo(new Barcelona());
			
			break;
		case 2:
			jug.setEquipo(new Juventus());
		default:
			break;
		}
	
		
		
		
		System.out.println(jug.getNombre()+"-"+ jug.getEquipo().mostrar()+" "+jug.getCamiseta().getNumero()+" "+jug.getCamiseta().getMarca().getNombre());

		((ConfigurableApplicationContext) appContext).close();
	}

}
