package ufro.dis.spring;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ufro.dis.beans.Barcelona;
import ufro.dis.beans.Camiseta;
import ufro.dis.beans.Jugador;
import ufro.dis.beans.Juventus;
import ufro.dis.beans.Marca;

@Configuration
public class AppConfig {
	@Bean
	public Jugador jugador1(){
		return new Jugador();
	}
	
	@Bean
	public Barcelona barcelona(){
		return new Barcelona();
	}
	
	@Bean
	public Juventus juventus(){
		return new Juventus();
	}
	
	
	@Bean
	public Camiseta camiseta(){
		return new Camiseta();
	}
	
	@Bean
	public Marca marca(){
		return new Marca();
	}
	
}
